# Backend Masterclass with Golang

### Handling migrations

To handle migrations, use the migrations library acquired from:
`https://github.com/golang-migrate/migrate/tree/master/cmd/migrate`

#### Getting started with migrations

Run this command: `migrate create -ext sql -dir db/migration -seq init_schema`

Populate and run init migrations scripts in `db` dir

To migrate, use the files in the `db/migration` folder or run the `migrateup` or `migratedown` commands in the `Makefile`
